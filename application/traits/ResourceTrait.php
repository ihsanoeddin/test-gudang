<?php
namespace traits;
use Illuminate\Database\Eloquent\Model as Eloquent;

trait ResourceTrait {

	protected $allowed_attributes = array();
	protected $resource_model;
	protected $resource;
	protected $use_resource = false;
	protected $options = array();
	protected $is_nested_resource = false;

	protected function resource_attributes($data = array())
	{
		$allowed_inputs = array();
		$data = empty($data) ? $this->input->post() : $data;
		foreach ($data as $attr => $value) {
			if (in_array($attr, $this->allowed_attributes)) $allowed_inputs[$attr] =$value;
		}
		return $allowed_inputs;
	}

	protected function _set_resource_attributes($except=array())
	{
		foreach ($this->resource_attributes() as $attr => $value) {
			if (!$this->_exception_params($except,$attr))
			{
				$this->resource->$attr = empty($value) ? $this->resource->$attr : $value;
			}
		}
	}

	protected function _exception_params($except=array(), $except_key)
	{
		foreach ($except as $key) {
			if ($key == $except_key)
			{
				return true;
			}
		}
	}

	protected function _find_resource()
	{
		try{
				$this->resource = call_user_func_array(array($this->resource_model, 'findOrFail'), array($this->uri->rsegment(3)));
		}catch(\Exception $e){
			show_404();
		}
	}

	protected function _new_resource()
	{
		$this->resource = new $this->resource_model;
	}

	protected function resource_data()
	{
		$post = is_null($this->input->post(underscore($this->resource_model))) ? [] : $this->input->post(underscore($this->resource_model));
		$files = $this->input->file(underscore($this->resource_model));
		$data = $post;
		if (!empty( $files ))
		{
			$data = array_merge_recursive($post, $files);
		}
		return $data;
	}
	protected function _resource()
	{
		$resource = is_null($this->resource_model) ? null : $this->resource_model;
		if (class_exists($resource))
		{
			$id = $this->router->uri->rsegment($this->is_nested_resource ? 4 : 3);

			if ( is_null($id) )
			{
				$this->resource = new $resource;
			}
			else
			{
				try{
						$this->resource = call_user_func_array(array($this->resource_model, 'findOrFail'), array($id));
				}catch (\Exception $e){
					show_404();
				}
			}
		}
	}
}