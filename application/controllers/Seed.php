<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Cartalyst\Sentry\Users\Eloquent\User as SentryModel;

class Seed extends Command
{
  protected $user;
  protected $userProvider;
  protected $group = array();
  protected $sentry;

  public function __construct()
  {
    parent::__construct();
    $this->sentry = Sentry::createSentry();
    $this->userProvider = $this->sentry->getUserProvider();
    SentryModel::setLoginAttributeName('username');
  }

  public function index()
  {
    try{
      $this->run(array('warehouses', 'categories'));
    }catch(Exception $e){
      echo $e->getMessage();
    }

  }

  protected function run($methods=array())
  {
    foreach ($methods as $method) {
      $this->$method();
    }
  }

  protected function warehouses()
  {
    if (Warehouse::all()->isEmpty())
    {
      $warehouses = array(
        array('code' => 'WH-1', 'name' => 'Warehouse 1'),
        array('code' => 'WH-2', 'name' => 'Warehouse 2'),
        array('code' => 'WH-3', 'name' => 'Warehouse 3')
        );
      foreach ($warehouses as $warehouse) {
        Warehouse::firstOrCreate($warehouse);
      }
    }
  }

  protected function categories()
  {
    if (Category::all()->isEmpty())
    {
      $categories = array(
        array('code' => 'SN', 'name' => 'Snack'),
        array('code' => 'HD', 'name' => 'Hardware'),
        array('code' => 'BV', 'name' => 'Beverage')
        );
      foreach ($categories as $category) {
        Category::firstOrCreate($category);
      }
    }
  }

}