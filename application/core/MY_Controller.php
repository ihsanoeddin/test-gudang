<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
  use traits\BeforeFilterTrait, traits\ResourceTrait;

    public function __construct()
    {
      parent::__construct();
      $this->filter_called_method();
      $this->form_validation->set_error_delimiters('<span class="help-inline error-validation-message">', '</span>');
    }

    protected function _request($type)
  {
    return strtolower($this->input->server('REQUEST_METHOD')) === strtolower($type);
  }

  protected function _only_ajax()
  {
    if (!$this->input->is_ajax_request())
    {
      show_404();
    }
  }

}
