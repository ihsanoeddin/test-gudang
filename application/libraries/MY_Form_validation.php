<?php

class MY_Form_validation extends CI_Form_validation {

    function edit_unique($value, $params)
    {
        $this->CI->load->database();

        $this->CI->form_validation->set_message('edit_unique', "Sorry, that %s is already being used.");

        list($table, $field, $current_id) = explode(".", $params);

        $query = $this->CI->db->select()->from($table)->where($field, $value)->limit(1)->get();

        if ($query->row() && $query->row()->id != $current_id)
        {
            return FALSE;
        }
    }

}