<div class="col-md-12">
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button> <a class="navbar-brand" href="#">Ezy test</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="">
                    <?php echo anchor('categories','Categories') ?>
                </li>
                <li class="">
                    <?php echo anchor('warehouses','Warehouses') ?>
                </li>
                <li>
                    <?php echo anchor('mutations','Mutations') ?>
                </li>
            </ul>
        </div>

    </nav>
</div>